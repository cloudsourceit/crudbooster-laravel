<?php

namespace cloudsourceit\crudbooster\middlewares;

use Closure;
use CRUDBooster;
use Request;

class CBBackend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $admin_path = config('crudbooster.ADMIN_PATH') ?: 'admin';
        if(!strpos(Request::path(), 'notifications/latest-json')){
            if (CRUDBooster::myId() == '') {
                if(Request::segment(1) != $admin_path)
                    $url =url(Request::segment(1).'/'.$admin_path.'/login');
                else
                    $url =url($admin_path.'/login');

                return redirect($url)->with('message', trans('crudbooster.not_logged_in'));
            }
            if (CRUDBooster::isLocked()) {
                $url = url($admin_path.'/lock-screen');

                return redirect($url);
            }
        }

        return $next($request);
    }
}
